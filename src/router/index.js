import { createRouter, createWebHistory } from '@ionic/vue-router';
// import store from "../store/index";

import CaleAI from '../pages/caleAI.vue';
import FlowPage from '../pages/FlowPage.vue';

const routes = [
  {
    path: '/',
    component: CaleAI
    // redirect: '/'
  },
  {
    path: '/member',
    component: FlowPage,
    meta: { requiredAuth: true },
    // redirect: '/'
  },
  {
    path: '/holismdsc',
    component: () => import('../components/holismdsc/agreeMent.vue')
  },
  {
    path: '/about',
    component: () => import('../components/holismdsc/aboutCaleAI.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router

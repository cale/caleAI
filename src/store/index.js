// import Vue from 'vue'
import { createStore } from 'vuex';
// import { Filesystem, Directory } from '@capacitor/filesystem';
// import { Storage } from '@capacitor/storage';

const store = createStore({
  state() {
    return {
      loginStatus: '',
      authData:
      {
        token: "",
        refreshToken: "",
        tokenExp: "",
        peerId: 0,
        peerName: "AJ",
      },
    }
  },
  mutations: {
  },
  actions: {
    async actionAuthStart() {
      // need to call parse
    }
  },
  getters: {
  },
});

export default store;
